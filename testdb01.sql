\echo '** creating database postgres-db..'

-- Create database postgres-db
CREATE DATABASE postgres-db WITH OWNER = postgres ENCODING = 'UTF8'  TABLESPACE = pg_default CONNECTION LIMIT = -1;

\echo '** finish creating postgres-db..'
